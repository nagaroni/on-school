export default (route) => {
  let { course_id, topic_id } = route.params
  let { lesson_id } = route.query

  return { course_id, topic_id, lesson_id }
}
