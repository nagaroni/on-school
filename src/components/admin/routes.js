import CourseNew from '@admin/courses/CourseNew'
import AdminCourseList from '@admin/courses/AdminCourseList'
import CourseEditor from '@admin/courses/CourseEditor'
import TopicsList from '@admin/courses/TopicsList'
import CoursePanel from '@admin/courses/CoursePanel'
import CourseDetails from '@admin/courses/CourseDetails'
import TopicPage from '@admin/courses/TopicPage'
import MainSection from '@base/MainSection'
import Navbar from '@base/Navbar'
import EditorProps from '@admin/courses/mixins/EditorProps'

export default [
  {
    path: 'courses',
    component: MainSection,
    children: [
      { path: '/', component: AdminCourseList },
      { name: 'newCourse', path: 'new', component: CourseNew },
      { name: 'coursePage', path: ':course_id', component: CoursePanel, children: [
          { name: 'courseDetails', path: 'details', component: CourseDetails },
          { name: 'courseTopics', path: 'topics', component: TopicsList },
          { name: '', path: 'topics/:topic_id', component: MainSection, children: [
              { name: 'courseTopic', path: '/', component: TopicPage },
              { name: 'courseEditor', path: 'editor', component: CourseEditor, props: EditorProps }
            ]
          },
        ]
      }
    ]
  }

]
