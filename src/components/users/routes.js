import UserCourseList from '@/components/users/UserCourseList'

export default [
  { path: '/', component: UserCourseList }
]
