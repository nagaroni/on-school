import { Attachment } from 'trix';
import FileUploader from '@/services/FileUploader'


const AttachmentButtonAttributes = {
  'type': 'button',
  'id': 'attach-button',
  'class': 'trix-button trix-button--icon trix-button--icon-attach',
  'data-trix-action': 'x-attach',
  'title': 'Attach a file',
  'tableindex': '-1'
}

const updateVideo = (attachment, data) => {
  attachment.setAttributes({ content: `
    <video width='800' height='500' controls><source src='${data.href}'></src></div>
    `})
}

const setAttributes = (btn) => {
  let attributes = Object.entries(AttachmentButtonAttributes)
  attributes.forEach(([attrName, attrValue]) => {
    btn.setAttribute(attrName, attrValue)
  })
}

const addToToolbar = (toolBar, trix, btn) => {
  let uploadButton = toolBar.querySelector('.trix-button-group.trix-button-group--block-tools')
    .appendChild(btn);
  uploadButton.addEventListener('click', function() {
    let fileInput = document.createElement("input");
    fileInput.setAttribute("type", "file");
    fileInput.setAttribute("multiple", "");

    fileInput.addEventListener("change", function(event) {
      var file, _i, _len, _ref, _results;
      _ref = this.files;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];
      }
      let attfile = new Attachment({content: "<video width='800' height='500' controls><source src=''></src></div>"})
      attfile.setFile(file)
      _results.push(trix.editor.insertAttachment(attfile));
      return _results;
    })

    fileInput.click()
  });
}

const AttachmentButton = (trix) => {
  if (document.getElementById('attach-button')) {
    return false;
  }
  let toolBar = trix.toolbarElement
  let btn = document.createElement('button')
  btn.insertText = 'Attach File'
  setAttributes(btn)
  addToToolbar(toolBar, trix, btn)
}

const uploadFile = (attachment, params) => {
  let file = attachment.file
  let  { course_id, lesson_id, topic_id } = params
  let progressUpdater = (percentCompleted) => {
    attachment.setUploadProgress(percentCompleted)
  }
  let path = `/admin/courses/${course_id}/lessons/${lesson_id}/attachment`

  return FileUploader.upload(file, 'attachment', path, progressUpdater)
};

export { AttachmentButton, uploadFile, updateVideo }
