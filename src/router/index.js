import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import HomePage from '@/components/HomePage'
import adminRoutes from '@/components/admin/routes.js'
import userRoutes from '@/components/users/routes.js'
import RegisterForm from '@/components/RegisterForm'
import Navbar from '@base/Navbar'
import MainSection from '@base/MainSection'
import Login from '@base/Login'
import { hasSession } from '@/services/Authenticate'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: Dashboard,
      children: [
        { path: '/', component: HomePage },
        { path: 'login', component: Login },
        { path: 'register', component: RegisterForm },
        {
          path: 'admin',
          component: MainSection,
          children: adminRoutes,
          meta: { requiresAuth: true }
        }, {
          path: 'my-dashboard',
          component: MainSection,
          children: userRoutes,
          meta: { requiresAuth: true }
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (!hasSession()) {
      next({
        path: '/login',
        query: { redirect_to: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router;
