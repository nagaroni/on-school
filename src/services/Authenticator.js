const Authenticate = (payload, router) => {
  Api.post('/login', payload).then(response => {
    let { user } = response.data
    localStorage.setItem('user.token', user.token)
    localStorage.setItem('user.name', user.name)

    Api.defaults.headers.common['Authorization'] = user.token
    router.push('/admin/courses')
  })
}


const hasSession = () => !!localStorage.getItem('user.token');

export default { Authenticate, hasSession };
