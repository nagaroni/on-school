class LineFactory {
  static build() {
    let newLine = document.createElement('div')
    let textContent = document.createElement('span')
    newLine.classList.add('newline')
    textContent.classList.add('text-node')
    newLine.appendChild(textContent)
    return newLine
  }
}
class Editor {
  constructor(element) {
    this.caretIndex = 0
    this.text = []
    this.element = element
    this.cursor = null
  }

  init() {
    this.addCursor()
    if(this.lines.length > 0) {

    } else {
      let newline = LineFactory.build()
      this.element.insertBefore(newline, window.blinking_cursor)
    }
  }

  lines() {
    return this.element.getElementsByClassName('newline')
  }

  addCursor() {
    this.cursor = document.createElement('div')
    this.cursor.classList.add('cursor-placeholder')
    this.element.parentElement.appendChild(this.cursor)
  }

  cursorBoundingRect() {
    return this.cursor.getBoundingClientRect()
  }

  updateCursor() {
    let cursorRec = this.cursorBoundingRect()
    let delta = cursorRec.height / 4.0;
    let blink = document.getElementById('blinking_cursor')
    blink.style.top = cursorRec.top
    blink.style.left = cursorRec.left - delta
  }

  linesSize() {
    return this.lines().length
  }

  lastLine() {
    return this.lines()[this.linesSize() - 1]
  }

  addLine(e) {
    let newline = LineFactory.build()
    this.element.insertBefore(newline, window.blinking_cursor)
  }

  appendToLine(target) {
    let last = this.lastLine()
    last.innerHTML = last.innerHTML + target
  }

  push(event) {
    event.preventDefault()
    let charString = String.fromCharCode(event.which)
    this.appendToLine(charString)
    this.text.push(charString)
    this.updateCursor()
  }
}

export default Editor;
