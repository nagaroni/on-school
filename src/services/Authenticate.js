const Authenticate = (payload, func) => {
  Api.post('/login', payload).then(response => {
    let user = response.data.user
    setSession(user)
    Api.defaults.headers.common['Authorization'] = user.token
    dispatchEvent(user)
    func(user)
  })
}

const setSession = (user) => {
  localStorage.setItem('user.token', user.token)
  localStorage.setItem('user.name', user.name)
}

const dispatchEvent = function (user = {}) {
  let updateEvent = new Event('update-session')
  updateEvent.user = user
  this.dispatchEvent(updateEvent)
}.bind(window)

const destroySession = (func) => {
  Api.delete('/logout').then(response => {
    clearSession()
    dispatchEvent()
    func()
  })
}

const clearSession = () => {
  localStorage.removeItem('user.token')
  localStorage.removeItem('user.name')
}

const hasSession = () => !!localStorage.getItem('user.token');

export { Authenticate, hasSession, destroySession }
