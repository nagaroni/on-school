class FileUploader {
  static upload(file, attrName, path, progressUpdater) {
    let data = new Formdata()
    data.append(params.attrName, file)
    data.append("Content-Type", file)

    let config = {
      onUploadProgress: function(progressEvent) {
        let percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total  );
        progressUpdater(percentCompleted)
      }
    }

    return Api.put(path, data, config)
  }
}

export default FileUploader
