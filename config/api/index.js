const axios = require('axios')

const token = localStorage.getItem('user.token')

const client = axios.create({
  baseURL: process.env.API_URL
})


client.defaults.headers.common['Authorization'] = token

module.exports = client
